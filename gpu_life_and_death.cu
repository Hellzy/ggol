#define DEAD 0
#define NEWBORN 1
#define ALIVE 2

#include <vector>

__global__ void life_and_death_kernel(int* cells, size_t size, size_t width, size_t height)
{
    int idx = blockIdx.x * blockDim.x + threadIdx.x;

    size_t x = idx % width;
    size_t y = idx / width;

    if (x == 0 || x >= width)
        return;
    if (y == 0 || y >= height)
        return;

    unsigned count = 0;
    auto state = cells[idx];

    if (x != 0)
        count += cells[y * width + x - 1] != DEAD;
    if (x < width - 1)
        count += cells[y * width + x + 1] != DEAD;
    if (y < height - 1 && x != 0)
        count += cells[(y + 1) * width + x] != DEAD;
    if (y < height - 1 && x < width - 1)
        count += cells[(y + 1) * width + x + 1] != DEAD;
    if (x != 0 && y < height - 1)
        count += cells[(y + 1) * width + x - 1] != DEAD;
    if (y != 0)
        count += cells[(y - 1) * width + x] != DEAD;
    if (y != 0 && x < width - 1)
        count += cells[(y - 1) * width + x + 1] != DEAD;
    if (y != 0 && x != 0)
        count += cells[(y - 1) * width + x - 1] != DEAD;

    __syncthreads();

    if (count == 3)
        cells[idx] = state == NEWBORN ? ALIVE : NEWBORN;
    else if (count < 2 || count > 3)
        cells[idx] = DEAD;
}

void life_and_death_gpu_wrapper(std::vector<int>& cells, size_t width, size_t height)
{
    /* Choose default device */
    cudaSetDevice(0);
    /* Reasonnable threadpool size */
    size_t poolSize = 1024;
    size_t poolNb = cells.size() / poolSize + 1;

    int* array = &cells[0];
    int* dev_cells = nullptr;

    cudaMalloc(&dev_cells, cells.size() * sizeof(int));
    cudaMemcpy(dev_cells, array, cells.size() * sizeof(int), cudaMemcpyHostToDevice);

    life_and_death_kernel<<<poolNb, poolSize>>>(dev_cells, cells.size(), width, height);

    cudaMemcpy(array, dev_cells, cells.size() * sizeof(int), cudaMemcpyDeviceToHost);
    cudaFree(dev_cells);

    cells.assign(array, array + cells.size());
}
