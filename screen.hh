#pragma once
#define DEAD 0
#define NEWBORN 1
#define ALIVE 2

#include <cstddef>
#include <SDL.h>
#include <vector>

class Screen
{
public:
    Screen(size_t width, size_t height);
    ~Screen();

    void populate(size_t cells_nb) noexcept;

    template<bool GPU = false>
    void cycle()
    {
        life_and_death();
        render();
    }

    void life_and_death() noexcept;
    void gpu_life_and_death() noexcept;

private:
    void render() noexcept;

private:
    size_t width_;
    size_t height_;
    std::vector<int> cells_;
    SDL_Window* window_ = nullptr;
    SDL_Renderer* renderer_ = nullptr;
};

template<>
inline void Screen::cycle<true>()
{
    gpu_life_and_death();
    render();
}
