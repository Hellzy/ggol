#include <signal.h>
#include <unistd.h>
#include <iostream>

#include "screen.hh"

static void sigint_handler([[maybe_unused]] int s)
{
    exit(0);
}

int main(int argc, char* argv[])
{
    Screen s(800, 600);
    s.populate(50000);

    if (argc > 2)
    {
        std::cerr << "Usage: " << argv[0] << " '[--gpu]'\n";
        return 1;
    }

    signal(SIGINT, sigint_handler);
    bool GPU = argc != 1;

    if (argc != 1)
        GPU |= std::string(argv[1]) == std::string("--gpu");

    while (true)
    {
        if (GPU)
            s.cycle<true>();
        else
            s.cycle();
    }

    return 0;
}
